# hellogolang

_hellographql_ it's a project developed in order to learn
 how to develop an application with Golang.

This project, for convenience and to expedite the development, uses a Mongodb DB.

### Requirements 📋

```
Go 1.15.6
```

### Build 🔧

To build this project we must execute the following command:

```
go build *.go
```

## Deployment 📦

To deploy the project, first of all, we must deploy
a Docker image that contains the Mongodb DB. For this we execute the following command:

```
docker run -d -p 27017:27017 --name hellogolang bitnami/mongodb
```

Once the DB is deployment, we proceed to deploy the project:

```
go run *.go
```

## Build with 🛠️

* [Golang](https://golang.org/) 
* [Gorilla/mux](https://github.com/gorilla/mux) 
* [mgo - Mongodb driver](https://labix.org/mgo) 

---
⌨️ with ❤️ by [franvallano](https://www.linkedin.com/in/francisco-javier-delgado-vallano-b28b1670/)
